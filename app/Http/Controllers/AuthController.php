<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
	public function register(Request $request)
	{
		// validasi - email harus unik
		$request->validate([
			'name' => 'required',
			'email' => 'required|unique:users,email|email:dns|regex:/gmail/',
			'password' => 'required',
			'photo_profile' => 'required|mimes:jpg,jpeg,png'
		]);

		// store photo_profile to local storage
		$photo_profile = $request->file('photo_profile');
		$path = $photo_profile->store('photo-profile');

		// hashing password
		$password = Hash::make($request->password);

		// simpan ke database
		// $user = new User();
		// $user->name = $request->name;
		// $user->email = $request->email;
		// $user->password = $password;
		// $user->save();

		$user = User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => $password,
			'photo_profile' => $path,
		]);

		// generate token
		$token = $user->createToken('elibrary-token')->plainTextToken;

		// kirim response
		return response()->json([
			'message' => 'success',
			'data' => $user,
			'token' => $token
		]);
	}

	public function login(Request $request)
	{
		// validasi - required, email exists
		$request->validate([
			'email' => 'required|exists:users,email',
			'password' => 'required'
		]);

		// validasi - email and password is match
		$user = User::where('email', $request->email)->first();
		$isPasswordMatch = Hash::check($request->password, $user->password);

		if (!$isPasswordMatch) {
			return response()->json([
				'message' => 'error',
				'errors' => "Password doesn't match",
			]);
		}

		$token = $user->createToken('elibrary-token')->plainTextToken;

		// send response: data user and token
		return response()->json([
			'message' => 'success',
			'data' => $user,
			'token' => $token,
		]);
	}
}
